from django.db import models
from django.contrib.auth.models import User
from django.template import Context, Template


class Card(models.Model):

    template = models.ForeignKey("CardTemplate", related_name="cards")
    user = models.ForeignKey("CardUser", related_name="cards")

    def render(self):
        t = Template(self.template.getTemplate())
        c = Context(self.user.getContext())
        return t.render(c)


class CardTemplate(models.Model):

    template_file = models.FileField(upload_to="templates/%Y/%m/%d")
    author = models.ForeignKey(User, related_name="templates")

    def getTemplate(self):
        template_data = self.template_file.read()
        return template_data


class CardUser(models.Model):
    first_name = models.TextField(max_length=30)
    last_name = models.TextField(max_length=30)
    phone_no = models.TextField(max_length=20)
    email = models.EmailField()
    address = models.TextField(max_length=100)

    def getContext(self):
        context_data = {}
        for field in self._meta.get_all_field_names():
            context_data[field] = getattr(self, field)
        return context_data

