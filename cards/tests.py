"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.contrib.auth.models import User
from selenium import webdriver
from cards.models import Card, CardTemplate, CardUser
import cStringIO
from django.core.files.uploadedfile import InMemoryUploadedFile


def create_file(content, name, field_name, content_type):
    out = cStringIO.StringIO()
    out.write(content)
    return InMemoryUploadedFile(
      file=out,
      field_name=field_name,
      name=name,
      content_type=content_type,
      size=out.tell(),
      charset=None)


class CardUserModelTest(TestCase):
    #TODO: add more attributes to user. Profile pic, etc
    def setUp(self):
        django_user = User.objects.create_user("tester",
                                        "tester@example.com",
                                        "testpassword")
        django_user.save()
        self.user = django_user

    def test_can_create_user(self):
        card_user = CardUser()
        card_user.user = self.user
        card_user.first_name = "Tester"
        card_user.last_name = "Testing"
        card_user.email = "tester@example.com"
        card_user.address = "test building, test street, test city"
        card_user.phone_no = "982829292"
        card_user.save()

        all_users_in_database = CardUser.objects.all()
        self.assertEquals(len(all_users_in_database), 1)
        only_user_in_database = all_users_in_database[0]
        self.assertEquals(only_user_in_database, card_user)
        self.assertEquals(only_user_in_database.first_name, "Tester")
        self.assertEquals(only_user_in_database.email, "tester@example.com")

    def test_get_context(self):
        card_user = CardUser()
        card_user.user = self.user
        card_user.first_name = "Tester"
        card_user.last_name = "Testing"
        card_user.email = "tester@example.com"
        card_user.address = "test building, test street, test city"
        card_user.phone_no = "982829292"
        card_user.save()

        user_from_database = CardUser.objects.get()
        context_data = user_from_database.getContext()
        self.assertEquals(context_data['first_name'], card_user.first_name)
        self.assertEquals(context_data['last_name'], card_user.last_name)
        self.assertEquals(context_data['email'], card_user.email)
        self.assertEquals(context_data['phone_no'], card_user.phone_no)
        self.assertEquals(context_data['address'], card_user.address)


class CardTemplateModelTest(TestCase):
    #TODO:Change so that only authors can create/edit templates

    def setUp(self):
        django_user = User.objects.create_user("tester", "tester@example.com",
                                        "testpassword")
        django_user.save()
        self.user = django_user

    def test_can_create_template(self):
        template = CardTemplate()

        #Create a test template
        template_data = """
                           {{user.first_name}}
                           {{user.last_name}}
                           {{user.email}}
                           {{user.phone_no}}
                           {{user.gender}}
                           {{user.address}}
                        """
        template.template_file = create_file(template_data,
                                            "template.html",
                                            "template_file",
                                            "text/html")
        template.author = self.user
        template.save()

        all_templates_in_database = CardTemplate.objects.all()
        self.assertEqual(len(all_templates_in_database), 1)
        only_template_in_database = all_templates_in_database[0]
        template_file = only_template_in_database.template_file
        result = template_file.read()
        self.assertEquals(result, template_data)
        self.assertEquals(only_template_in_database.author, self.user)

    def test_get_template(self):
        template = CardTemplate()

        #Create a test template
        template_data = """
                           {{user.first_name}}
                           {{user.last_name}}
                           {{user.email}}
                           {{user.phone_no}}
                           {{user.gender}}
                           {{user.address}}
                        """
        template.template_file = create_file(template_data,
                                            "template.html",
                                            "template_file",
                                            "text/html")
        template.author = self.user
        template.save()

        template_from_database = CardTemplate.objects.get()
        self.assertEquals(template_from_database.getTemplate(), template_data)


class CardModelTest(TestCase):
    #TODO: add stylesheet to template
    def setUp(self):
        django_user = User.objects.create_user("tester", "tester@example.com",
                                        "testpassword")
        django_user.save()
        user = CardUser()
        user.first_name = "tester"
        user.last_name = "testing"
        #user.gender = "M"
        user.email = "tester@example.com"
        user.phone_no = "999999999"
        user.address = "test building, test street, test city"
        #user.position = "Application Test"
        user.save()
        self.user = user
        template = CardTemplate()
        template_data = """
                           {{first_name}}
                           {{last_name}}
                           {{email}}
                           {{phone_no}}
                           {{address}}
                        """
        template.template_file = create_file(template_data,
                                            "template.html",
                                            "template_file",
                                            "text/html")
        author = User.objects.create_user("test_designer",
                                    "designer@example.com",
                                    "testpassword")
        author.save()
        template.author = author
        template.save()
        self.template = template

    def tearDown(self):
        pass

    def test_can_create_card(self):
        card = Card()
        card.template = self.template
        card.user = self.user
        card.save()

        all_cards_in_database = Card.objects.all()
        self.assertEquals(len(all_cards_in_database), 1)
        only_card_in_database = all_cards_in_database[0]
        self.assertEquals(only_card_in_database, card)

        self.assertEquals(only_card_in_database.template, self.template)
        self.assertEquals(only_card_in_database.template, self.template)

    def test_can_delete_card(self):
        card = Card()
        card.template = self.template
        card.user = self.user
        card.save()

        card.delete()
        all_cards_in_database = Card.objects.all()
        self.assertEquals(len(all_cards_in_database), 0)

    def test_can_render_card(self):
        card = Card()
        card.template = self.template
        card.user = self.user
        card.save()

        result = card.render()
        self.assertIn(card.user.first_name, result)
        self.assertIn(card.user.last_name, result)
        self.assertIn(card.user.email, result)
        self.assertIn(card.user.phone_no, result)
        self.assertIn(card.user.address, result)
