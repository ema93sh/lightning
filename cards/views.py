from django.views.generic import View
from django.shortcuts import get_object_or_404
from models import Card


class CardView(View):

    def get(self, request, *args, **kwargs):
        card = get_object_or_404(Card, pk=self.args[0])
        card_html = card.render()
        return render(request, "card/view", card=card, card_html=card_html)
